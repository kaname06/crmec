## Description

REST Api to CRM and EC system.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Only do it for me [Adrian]().

## Stay in touch

- Author - [Adrian Correa]()

## License

  CRM-EC is Adrian Correa Copyright.
