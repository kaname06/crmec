import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import iAuth from '../utils/interfaces/auth.interface';
import authDto from '../utils/dtos/auth.dto';
import { UserService } from '../user/user.service';
import UserDto from 'src/utils/dtos/user.dto';
import { ClientService } from '../client/client.service';
import ClientDto from 'src/utils/dtos/client.dto';

@Injectable()
export class AuthService {
    constructor(
        @InjectModel('Auth') private readonly AM: Model<iAuth>,
        private readonly jwt: JwtService,
        private readonly US: UserService,
        private readonly CS: ClientService
    ){}

    async login(nickname: string, password: string): Promise<{success: boolean, info: string, token?:string, data?: any}> {
        if(!nickname || !password || typeof nickname != 'string' || typeof password != 'string') {
            return {
                success: false,
                info: "Missing data"
            }
        }
        let user = await this.AM.findOne({nickname: nickname})
        if(!user || !user._id) {
            return {
                success: false,
                info: "Wrong credentials"
            }
        }
        if(!user.validatePass(password)) {
            return {
                success: false,
                info: "Wrong credentials"
            }
        }
        if(!user.status) {
            return {
                success: false,
                info: "Inactive user"
            }
        }
        let data = {
            user: await this.US.getUser(user.user.toString()),
            client: await this.CS.getClient(user.user.toString())
        }
        let token = await this.jwt.sign({_id: user._id, lastAccess: user.lastAccess, user: user.user})
        return {
            success: true,
            info: "Authenticated!",
            data,
            token
        }
    }

    async createAuth(userdata: UserDto, authdata: authDto, clientdata: ClientDto): Promise<{success: boolean, info: string | object}> {
        if(!authdata.nickname || !authdata.password) {
            return {
                success: false,
                info: "Missing auth data"
            }
        }

        let valiNick = await this.AM.findOne({nickname: authdata.nickname})

        if(valiNick && valiNick._id)
            return {
                success: false,
                info: "Nickname in use"
            }

        let userInfo = await this.US.createUser(userdata)

        if(!userInfo) {
            return {
                success: false,
                info: "Missing user data"
            }
        }

        if(!userInfo.success) {
            return userInfo
        }
        authdata.user = userInfo.data

        if(clientdata) {
            clientdata.user = userInfo.data
            let clientInfo = await this.CS.createClient(clientdata)

            while (!clientInfo) {
                clientInfo = await this.CS.createClient(clientdata)
            }

            if(!clientInfo.success) {
                await this.US.delUser(userInfo.data)
                return clientInfo
            }
        }
        //proceed to save
        let Data = new this.AM(authdata)
        Data.password = await this.AM.schema.statics.encryptPass(Data.password)
        var error = Data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return {
                success: false,
                info: status
            }             
        } else{
            let status = await Data.save();
            if(('_id' in status)) {
                return {
                    success: true,
                    info: 'User save successfuly'
                }
            } else {
                await this.US.delUser(userInfo.data)
                await this.CS.delClient(userInfo.data)
                return {
                    success: false,
                    info: 'An error ocurred while saving data',
                }
            }        
        }
    }

    async changePassword(user: string, password: string, newPassword: string) {
        if(!user || !password || !newPassword || !Types.ObjectId.isValid(user)) {
            return {
                success: false,
                info: "Missing information"
            }
        }
        let userData = await this.AM.findOne({user: user})
        if(!userData || !userData._id)
            return {
                success: false,
                info: "User not found"
            }
        if(!userData.validatePass(password))
            return {
                success: false,
                info: "Wrong password validation"
            }
        if(newPassword.length < 8)
            return {
                success: false,
                info: "New password too short"
            }
        let encPass = this.AM.schema.statics.encryptPass(newPassword)
        userData.password = encPass
        let result = await userData.save()
        if(!result || !result._id)
            return {
                success: false,
                info: "Something went wrong while saving"
            }
        return {
            success: true,
            info: "Password updated successfully"
        }
    }
}
