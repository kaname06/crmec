import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { MongooseModule } from '@nestjs/mongoose';
import AuthSchema from '../utils/schemas/auth.schema';
import { JwtModule } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import UserSchema from '../utils/schemas/user.schema';
import { ClientService } from '../client/client.service';
import ClientSchema from '../utils/schemas/client.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Auth', schema: AuthSchema},{name:'User', schema: UserSchema},{name: 'Client', schema: ClientSchema}]),
    JwtModule.register({
      secret: 'kaiokenx100kaiokenx200kaiokenx300xmiui!',
      signOptions: { expiresIn: '12h' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, UserService, ClientService]
})
export class AuthModule {}
