import { Controller, Post, Body, Put, Param } from '@nestjs/common';
import { AuthService } from './auth.service';
import authDto from '../utils/dtos/auth.dto';
import UserDto from '../utils/dtos/user.dto';
import ClientDto from '../utils/dtos/client.dto';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService
    ){}

    @Post('/create')
    async createAuthData(@Body('auth') authDTO: authDto, @Body('user') userDTO: UserDto, @Body('client') clientDTO: ClientDto){
        return await this.authService.createAuth(userDTO, authDTO, clientDTO)
    }

    @Post('/validate')
    async validateAuth(@Body('nickname') nickname, @Body('password') password) {
        return await this.authService.login(nickname, password)
    }

    @Put('/update/password/:user')
    async updatePassword(@Body('password') password: string, @Body('newPassword') newPassword: string, @Param('user') user: string) {
        return await this.authService.changePassword(user, password, newPassword)
    }
}
