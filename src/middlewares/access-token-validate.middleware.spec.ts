import { AccessTokenValidateMiddleware } from './access-token-validate.middleware';

describe('AccessTokenValidateMiddleware', () => {
  it('should be defined', () => {
    expect(new AccessTokenValidateMiddleware()).toBeDefined();
  });
});
