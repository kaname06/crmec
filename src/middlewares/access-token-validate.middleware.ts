import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AccessTokenValidateMiddleware implements NestMiddleware {
  constructor(private jwt: JwtService){}
  use(req: Request, res: Response, next: NextFunction) {
    let token = <string>req.headers["x-a-t"]
    if(!token) {
      return res.json({
        info: "Missing token"
      })
    }
    if(!this.jwt.verify(token)) {
      return res.json({
        info: "Access Denied"
      })
    }
    next();
  }
}
