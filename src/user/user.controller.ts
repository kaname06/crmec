import { Controller, Get, Body } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private userService: UserService){}

    @Get('/validateExist')
    async GetUserByParams(@Body('params') params: any) {
        return await this.userService.getUserByParams(params)
    }
}
