import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import iUser from '../utils/interfaces/user.interface';
import UserDto from '../utils/dtos/user.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectModel('User') private readonly UM: Model<iUser>
    ){}

    async createUser(userInfo: UserDto): Promise<{success: boolean, info: string | object, data?: string} | null> {
        if(!userInfo)
            return null
        let vali = await this.UM.findOne({$or: [{mail: userInfo.mail}, {'dni.dni': userInfo.dni.dni}]})
        if(vali && vali._id)
            return {
                success: false,
                info: "Mail or dni already exist"
            }
        //proceed to save
        let Data = new this.UM(userInfo)
        var error = Data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return {
                success: false,
                info: status
            }             
        } else{
            let status = await Data.save();
            if(('_id' in status)) {
                return {
                    success: true,
                    info: 'User save successfuly',
                    data: status._id
                }
            } else {
                return {
                    success: false,
                    info: 'An error ocurred while saving data',
                }
            }        
        }
    }

    async getUser(id: string) {
        const data = await this.UM.findById(id)
        return data
    }

    async delUser(id: string) {
        let data = await this.UM.findByIdAndRemove(id)
        if(!data || !data._id) {
            return {
                success: false,
                info : "Unable to delete this user now"
            }
        }
        return {
            success: true,
            info: "User deleted"
        }
    }

    async getUserByParams(params: any) {
        return await this.UM.findOne(params)
    }
}
