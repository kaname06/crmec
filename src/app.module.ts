import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';

import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ClientModule } from './client/client.module';
import { AccessTokenValidateMiddleware } from './middlewares/access-token-validate.middleware';
import { UserController } from './user/user.controller';
import { JwtModule } from '@nestjs/jwt';


@Module({
  imports: [ 
    MongooseModule.forRoot('mongodb://localhost:27017/crmec', {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }),
    JwtModule.register({
      secret: 'kaiokenx100kaiokenx200kaiokenx300xmiui!',
      signOptions: { expiresIn: '12h' },
    }),
    AuthModule,
    UserModule, 
    ClientModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
    .apply(AccessTokenValidateMiddleware)
    // .exclude()
    .forRoutes(UserController)
  }
}
