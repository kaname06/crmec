import { Controller, Post, UseInterceptors, UploadedFiles } from '@nestjs/common';
import { FileFieldsInterceptor } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import { extname } from "path";

@Controller('client')
export class ClientController {
    @Post('upload/file/:id')
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'hd', maxCount: 1},
        { name: 'rut', maxCount: 1},
        { name: 'cc', maxCount: 1}
    ], {
        storage: diskStorage({
            destination: './documents', 
            filename: (req, file, cb) => {
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                return cb(null, `${file.fieldname}-${randomName}${extname(file.originalname)}`)
            }
        }),
        fileFilter: function (req: any, file: any, cb: any): any {
            if (extname(file.originalname) !== '.pdf') {
                return cb(new Error('Only pdfs are allowed'))
            }
            cb(null, true)
        },
        limits: {
            fileSize: 1000000
        }
    }))
    uploadFiles(@UploadedFiles() files) {
        if(files) {
            let paths: string[] = []
            for (const key in files) {
                paths.push(files[key][0].path)
            }
            return {success: true, info: "some uploaded", data: paths}
        }
        return {success: false, info: "nothing uploaded"}
    }
}
