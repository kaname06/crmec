import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import iClient from '../utils/interfaces/client.interface';
import ClientDto from '../utils/dtos/client.dto';
import { unlink } from 'fs-extra';
import { resolve } from 'path';

@Injectable()
export class ClientService {
    constructor(
        @InjectModel('Client') private readonly CM: Model<iClient>
    ){}

    async getClient(id: string) {
        return await this.CM.findOne({user: id})
    }

    async createClient(client: ClientDto) {
        if(!client)
            return {
                success: false,
                info: "Missing data"
            }
        //proceed to save
        let Data = new this.CM(client)
        var error = Data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return {
                success: false,
                info: status
            }             
        } else{
            let status = await Data.save();
            if(('_id' in status)) {
                return {
                    success: true,
                    info: 'Client save successfuly',
                }
            } else {
                return {
                    success: false,
                    info: 'An error ocurred while saving data',
                }
            }        
        }
    }

    async updateClientInfo(id: string, infoclient: ClientDto) {
        let data = await this.CM.findByIdAndUpdate(id, infoclient, {new: true})
        if(!data || !data._id)
            return {
                success: false,
                info: "An error ocurred while saving data, try later"
            }
        return {
            success: true,
            info: "Client information updated",
            data
        }
    }

    async delClient(id: string) {
        let data = await this.CM.findOneAndRemove({user: id})
        if(!data || !data._id)
            return {
                success: false,
                info: "Error"
            }
        return {
            success: true,
            info: "Client delete successfully"
        }
    }

    async addDocsPath(id:string, paths: string[]) {
        if(!paths)
            return {
                success: false,
                info: "Cannot get paths"
            }
        if(!Types.ObjectId.isValid(id)) {
            return {
                success: false,
                info: "Invalid user"
            }
        }
        let client = await this.CM.findOne({user:id})
        if(!client || !client._id) {
            return {
                success: false,
                info: "User not found"
            }
        }
        let deleted: string[] = []
        for (let index = 0; index < paths.length; index++) {
            let isthere = false
            for (let ind = 0; ind < client.docs.length; ind++) {
                if(paths[index].split('-')[0] == client.docs[ind].split('-')[0]) {
                    isthere = true
                    deleted.push(client.docs[ind])
                    client.docs[ind] = paths[index]
                }
            }
            if(isthere)
                client.docs.push(paths[index])
        }
        let data = await client.save()
        while(!data) {
            data = await client.save()
        }
        for (let index = 0; index < deleted.length; index++) {
            if(deleted[index]) {
                await unlink(resolve(deleted[index]))
            }
        }
    }
}
