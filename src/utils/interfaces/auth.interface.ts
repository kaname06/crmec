import { Types, Document } from "mongoose";

export default interface iAuth extends Document {
    nickname: string;
    password: string;
    status: boolean;
    lastAccess: Date;
    user: Types.ObjectId;
    validatePass(password: string):boolean;
}