import { Document } from "mongoose";

export default interface iClient extends Document{
    user: string;
    contact: string;
    docs: string[];
    vendor: string;
    clientType: string;
    doctor?: {
        institution: string;
        service: string;
        jobTitle: string;
        collegeDegree: string;
    }
}