import { Document } from "mongoose";

export default interface iUser extends Document{
    name: string,
    dni: {
        dni: string;
        dniType: string;
    };
    Address?: {
        country?: string;
        town?: string;
        addInfo?: string;
    };
    phone?: string;
    mail?: string;
    bankData?: {
        Account: string;
        bank: string;
        AccountType: string;
    };
    userType: string;
}