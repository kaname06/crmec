export default class ClientDto {
    user: string;
    contact: string;
    vendor: string;
    clientType: string;
    doctor?: {
        institution: string;
        service: string;
        jobTitle: string;
        collegeDegree: string;
    }
}