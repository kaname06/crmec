export default class UserDto {
    readonly name: string;
    readonly dni: {
        readonly dni: string;
        readonly dniType: string;
    };
    readonly Address?: {
        readonly country?: string;
        readonly town?: string;
        readonly addInfo?: string;
    };
    readonly phone?: string;
    readonly mail?: string;
    readonly bankData?: {
        readonly Account: string;
        readonly bank: string;
        readonly AccountType: string;
    };
    readonly userType: string;
}