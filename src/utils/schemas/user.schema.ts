import { Schema } from "mongoose";

const UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    dni: {
        dniType: {
            type: String,
            required: true
        },
        dni: {
            type: String,
            required: true
        }
    },
    Address: {
        country: {
            type: String,
            default: null
        },
        town: {
            type: String,
            default: null
        },
        addInfo: {
            type: String,
            default: null
        }
    },
    phone: String,
    mail: String,
    bankData: {
        Account: {
            type: String,
            default: null
        },
        bank: {
            type: String,
            default: null
        },
        AccountType: {
            type: String,
            default: null
        }
    },
    userType: {
        type: String,
        default: 'vendor'
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

export default UserSchema