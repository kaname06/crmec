import { Schema } from "mongoose";

const ClientSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: true
    },
    contact: {
        type: String,
        required: true
    },
    vendor: {
        type: Schema.Types.ObjectId,
        required: true
    },
    docs: [String],
    clientType: String,
    doctor: {
        institution: String,
        jobTitle: String,
        service: String,
        collegeDegree: String
    }
})

export default ClientSchema