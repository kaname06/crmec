import { Schema } from "mongoose";
import * as bcrypt from "bcryptjs"

const AuthSchema = new Schema({
    nickname: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true
    },
    lastAccess: {
        type: Date,
        default: Date.now
    },
    status: {
        type: Boolean,
        default: false
    }
})

AuthSchema.statics.encryptPass = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

AuthSchema.methods.validatePass = function (password) {
    return bcrypt.compareSync(password, this.password)
} 


export default AuthSchema